import React from "react";
import { Card, CardActions, CardActionButtons } from 'rmwc/Card';
import { Typography } from 'rmwc/Typography';
import { TextField } from 'rmwc/TextField';
import { Button } from 'rmwc/Button';

class SignIn extends React.Component {

    SignIn = e => {
        e.preventDefault();
        const email = this.emailRef.value;
        const pass = this.passRef.value;
        this.props.SignIn(email,pass);
        e.currentTarget.reset();
    };  
    
    render() {
        return (
            <Card className = "SignForm">
                <form onSubmit={this.SignIn}>
                    <div className = "center"><Typography use="headline2">Tweeter</Typography></div>
                    <div className = "SignDiv">
                        <TextField box required label="Email" type="email" ref={input => this.emailRef = input}/>
                        <TextField box required label="Password" type="password" ref={input => this.passRef = input}/>
                    </div>
                    <CardActions className = "SignButtons">
                        <CardActionButtons>
                        <div className = "LinkButtons">
                            <Button dense type="Button" onClick={() => this.props.switchform("SignUp")}>Register</Button>
                            <Button dense type="Button" onClick={() => this.props.switchform("ResPass")}>Forgot Password</Button>
                        </div>
                        <Button raised type="Submit">Sign In</Button>
                        </CardActionButtons>
                    </CardActions>
                </form>
            </Card>
        );
  }
}

export default SignIn;
