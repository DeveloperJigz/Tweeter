import React from "react";
import { Grid, GridCell } from "rmwc/Grid";
import ActionMenu from "./ActionMenu";
import Feed from "./Feed";

class Page extends React.Component {
  render() {
    return (
        <Grid className="Page">
          <GridCell span="4"><ActionMenu PushTweets={this.props.PushTweets}/></GridCell>
          <GridCell span="8"><Feed tweets={this.props.tweets} user={this.props.user}/></GridCell>
        </Grid>
    );
  }
}

export default Page;
