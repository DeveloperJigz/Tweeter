import React from "react";
import SignIn from "./SignIn";
import SignUp from "./SignUp";
import ResPass from "./ResPass";

class Auth extends React.Component {

  state={
    form: "SignIn"
  };
  
  switchform = (form) => {
    this.setState({ form: form });
  };

    render() {
      return (
        <div className="Auth">
          {this.state.form === "SignIn" && <SignIn switchform = {this.switchform} SignIn = {this.props.SignIn}/>}
          {this.state.form === "SignUp" && <SignUp switchform = {this.switchform} SignUp = {this.props.SignUp}/>}
          {this.state.form === "ResPass" && <ResPass switchform = {this.switchform} ResPass = {this.props.ResPass}/>}
        </div>  
      );
    }
  }
  
  export default Auth;
