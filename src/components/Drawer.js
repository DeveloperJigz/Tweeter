import React from "react";
import { Drawer, DrawerHeader, DrawerContent } from "rmwc/Drawer";
import { ListItem, ListItemText } from "rmwc/List";
import { Typography } from 'rmwc/Typography';
import ImageWrap from "./ImageWrap";

class Menudrawer extends React.Component {
  render() {
    return (
        <Drawer persistent open={this.props.drawer} onClose={this.props.closedrawer} className="noselect">
          <DrawerHeader>
              <ImageWrap photoURL={this.props.User.photoURL}/>
              <Typography use="headline4" className="flex_block displayName">{this.props.User.displayName}</Typography>
              <Typography use="caption" className="flex_block displayEmail">{this.props.User.email}</Typography>
          </DrawerHeader>
          <DrawerContent>
            <ListItem>
              <ListItemText>Feed</ListItemText>
            </ListItem>
            <ListItem>
              <ListItemText>Profile</ListItemText>
            </ListItem>
            <ListItem>
              <ListItemText>Settings</ListItemText>
            </ListItem>
            <ListItem>
              <ListItemText onClick={() => this.props.LogOut()}>Logout</ListItemText>
            </ListItem>
          </DrawerContent>
          <Typography use="overline"><p className="flex_block drawer_footer">© 2018 Copyright: Tweeter</p></Typography>
        </Drawer>
    );
  }
}

export default Menudrawer;
