import React from "react";
import firebaseApp from "./../base";
import Navbar from "./Navbar";
import Page from "./Page";
import Auth from "./Auth";
import Snack from "./Snack";

class App extends React.Component {
  state = {
    isAuthenticating: true,
    user: null,
    drawer: false,
    snackbarIsOpen: false,
    snackMessage: "",
    tweets: []
  };

  PushTweets = (tweets) => {
    tweets = [...this.state.tweets, ...tweets];
    console.log(tweets);
    this.setState({tweets:tweets});
  }

  componentWillMount(){
    firebaseApp.auth().onAuthStateChanged((user)=>{
      console.log(user);
      if(user){
        this.setState({drawer:false});
        localStorage.setItem('user',user.uid);
        this.setState({user:user,isAuthenticating:false});
      }
      else{
        localStorage.removeItem('user');
        this.setState({user:null,isAuthenticating:false});
      }
    });
  }

  SignIn = (email, pass) => {
    const promise = firebaseApp.auth().signInWithEmailAndPassword(email, pass);
    promise.catch(e => this.showsnack(e.message));
  }
  SignUp = async (name, email, pass) => {
    await firebaseApp.auth().createUserWithEmailAndPassword(email, pass)
      .then(function() {}, function(error) {
      this.showsnack(error.message);
    });
    firebaseApp.auth().currentUser.updateProfile({
      displayName: name,
      photoURL: "https://firebasestorage.googleapis.com/v0/b/tweeter-956e7.appspot.com/o/default.jpg?alt=media&token=8044fd99-6f3f-4d6d-8f51-86136de879dc"
    })
      .then(function() {}, function(error) {
      this.showsnack(error.message);
    });
  }
  ResPass = (email) => {
    const promise = firebaseApp.auth().sendPasswordResetEmail(email);
    promise.catch(e => this.showsnack(e.message));
  }
  LogOut = () => {
    firebaseApp.auth().signOut()
      .then(function() {}, function(error) {
      this.showsnack(error.message);
    });
    this.setState({user:null});
  }

  showsnack = (message) => {
    this.setState({snackMessage: message,snackbarIsOpen: !this.state.snackbarIsOpen});
  }
  
  hidesnack = () => {
    this.setState({snackbarIsOpen: false});
  }

  toggledrawer = () => {
    this.setState({ drawer: !this.state.drawer });
  };

  render() {
    return (
      <React.Fragment>
        {!this.state.isAuthenticating && this.state.user && //show if user is logged in
          <main>
          <Navbar 
            drawer={this.state.drawer} 
            toggledrawer={this.toggledrawer} 
            LogOut={this.LogOut}
            User={this.state.user}
          />
          <div className={(this.state.drawer ? "padded " : "unpadded ")}>
            <Page tweets={this.state.tweets} user={this.state.user} PushTweets={this.PushTweets}/>
          </div>
        </main>
        }
        {!this.state.isAuthenticating && !this.state.user &&  //show if no user is logged in
          <Auth SignUp={this.SignUp} SignIn={this.SignIn} ResPass={this.ResPass}/>
        }
        <Snack snackbarIsOpen={this.state.snackbarIsOpen} hidesnack={this.hidesnack} snackMessage={this.state.snackMessage}/>
      </React.Fragment>
    );
  }
}

export default App;
