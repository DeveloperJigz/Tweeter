import React from "react";
import { Elevation } from 'rmwc/Elevation';

class ImageWrap extends React.Component {
  render() {
    return (
      <div className="flex_block">
        <Elevation z={6} className="image">
          <div className="image" style={{ backgroundImage: "url(" + this.props.photoURL + ")"}}></div>
        </Elevation>
      </div>
    );
  }
}

export default ImageWrap;
