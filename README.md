# Project TwitterSplitter

**Description**

The product Tweeter allows users to post short messages limited to 50 characters each.

Sometimes, users get excited and write messages longer than 50 characters.
  
Instead of rejecting these messages, we would like to add a new feature that will split the message into parts and send multiple messages on the user's behalf, all of them meeting the 50 character requirement.

**Example**

Suppose the user wants to send the following message:

```
I can't believe Tweeter now supports 
chunking my messages, so I don't have to do it myself.
```

This is 91 characters excluding the surrounding quotes. When the user presses send, it will send the following messages:

``` 
1/2 I can't believe Tweeter now supports chunking
```
``` 
2/2 my messages, so I don't have to do it myself.
```

Each message is now 49 characters, each within the allowed limit.

## Requirements

1. Create a web application that serves the Tweeter interface. It will support the following functionality:

* Allow the user to input and post messages.
* Display the user's messages.
* If a user's input is less than or equal to 50 characters, post it as is.
* If a user's input is greater than 50 characters, split it into chunks that each is less than or equal to 50 characters and post each chunk as a separate message.
* Messages will only be split on **whitespace**. If the message contains a span of non-whitespace characters longer than 50 characters, display an error.
* Split messages will have a **part indicator** appended to the beginning of each section. In the example above, the message was split into two chunks, so the part indicators read **1/2** and **2/2**. Be aware that these count toward the character limit.

2. The functionality that splits messages should be a standalone function. Given the above example, its function call would look like:

```
splitMessage("I can't believe Tweeter now supports chunking my
messages, so I don't have to do it myself.")
```

and it would return

```
1/2 I can't believe Tweeter now supports chunking 
2/2 my messages, so I don't have to do it myself.
```
3. The business logic (a.k.a. the message splitter) should be unit tested. We have to know it works, right?
4. The submission should be a git repository. In the project directory, `git log` should show your commits.
5. Bonus points for any additional polish and sophistication you add to the experience.



## Testing
Repository will be hosted on **GitLab**, and [Live Preview](https://tweeter.obscurance.com/) is made available through Obscurance.

## Splitting Function

``` 
    <TextField outlined label="Write your Tweet..." ref = {input => this.tweetRef = input}/>
    	
```


``` 
    const tweets = this.splitTweet(this.tweetRef.value.replace(/\s\s+/g, ' '), 50);
    	
```

```
splitTweet  = (tweet, max) => {
	const  words  =  tweet.split(' ');
	var  chunks  =  1; //assumes total chunks is in single digits (1-9)
	var  loop  =  true; //loop once if chunk digits change

	while(loop){
		var  indicator, last, tweets  = [""];
			for (const  word  in  words){
				last  =  tweets.length-1;
				indicator  =  2  + (last).toString().length +  chunks.toString().length;
				if(( tweets[last] +  words[word] ).length <  max  -  indicator)
					tweets[last] =  tweets[last] + (tweets[last].length >  0  ?  " "  :  "") +  words[word];
				else
					tweets.push( words[word] );
			}
		if((tweets.length).toString().length >  chunks.toString().length) //check if chunk digits changed
			loop  =  true;
		else
			loop  =  false;
		chunks  =  tweets.length;
	}
	
	for (const  tweet  in  tweets) // add indicators to tweets
		{tweets[tweet] =  Number(tweet)+1  +  "/"  +  chunks  +  " "  +  tweets[tweet];}
	return  tweets;
};

```

## Project Stack

Technologies and Tools used for Project:

|Function                         |Used                       |
|---------------------------------|---------------------------|
|Front End                    	  |`React.js(Netlify)`        |
|Back End                    	  |`Node.js(Heroku)`          |
|Git Repository                   |`GitLab(GitKraken)`        |
|Editor                           |`Visual Code`              |
|Browser                          |`Firefox Developer Edition`|






